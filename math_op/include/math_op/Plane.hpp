#ifndef Plane_HPP_
#define Plane_HPP_

//=================================
// Forward declared dependencies
//=================================
namespace MathOp { class Transform; }

//=================================
// Included dependencies
//=================================
#include <opencv2/core/core.hpp>

namespace MathOp
{

class Plane
{
public:
	Plane() {}

	int setParams(double a, double b, double c, double d);

	Plane transform(const MathOp::Transform& trans) const;


	// distanceTo calculates the signed distance
	// D>0 if the point p is in the half-space determined by the direction of the normal.
	double distanceTo(const cv::Point3d& p) const;

	cv::Vec3d getNormal() const { return cv::Vec3d(a_, b_, c_); }
	cv::Point3d getPointInPlane() const;
	double getA() const { return a_; }
	double getB() const { return b_; }
	double getC() const { return c_; }
	double getD() { return d_; }

private:
	//    * The model coefficients are defined as:
	//    *   a : the X coordinate of the plane's normal (normalized)
	//    *   b : the Y coordinate of the plane's normal (normalized)
	//    *   c : the Z coordinate of the plane's normal (normalized)
	//    *   d : the fourth <a href="http://mathworld.wolfram.com/HessianNormalForm.html">Hessian component</a> of the plane's equation
	double a_, b_, c_, d_;
};

}


#endif
